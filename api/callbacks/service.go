package callbacks

import (
	"dils-backend/dao"
	"dils-backend/utils/notifications"
	"log"
)

// Service represents the Callbacks Service
type Service struct {
	factoryDAO *dao.FactoryDAO
	notifiable notifications.Notifiable
}

// NewCallbacksService returns a new callbacks service
func NewCallbacksService(factoryDAO *dao.FactoryDAO) *Service {
	notifiable, err := notifications.NewNotifiable(factoryDAO)
	if err != nil {
		log.Fatalf("notifiable_init: %v", err)
		return nil
	}
	return &Service{factoryDAO: factoryDAO, notifiable: notifiable}
}
