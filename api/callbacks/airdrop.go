package callbacks

import (
	"dils-backend/models"
	"dils-backend/utils"
	"dils-backend/utils/notifications"
	"encoding/json"
	"errors"
	"fmt"
	"log"
	"net/http"
	"os"
	"strconv"
	"time"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
)

const (
	airdropReleaseAmount float64 = 1000
)

// RetrieveDropCode registers an email for an Airdrop. If email exists, a
// status[400] is thrown
func (s *Service) RetrieveDropCode(w http.ResponseWriter, r *http.Request) {
	var data map[string]interface{}
	err := utils.DecodeReq(r, &data)
	if err != nil {
		log.Printf("err decoding airdrop req: %v", err)
		utils.RespondWithError(w, http.StatusBadRequest, "Invalid request data detected")
		return
	}

	email := data["email"].(string)

	if email == "" {
		utils.RespondWithError(w, http.StatusBadRequest, "Invalid email address provided")
		return
	}

	// check email hasn't participated in airdrop
	dropExist, err := s.factoryDAO.Query("airdrop", bson.M{
		"email": email,
	})
	if err != nil {
		log.Printf("err retrieving airdrop: %v", err)
		utils.RespondWithError(w, http.StatusBadRequest, "Invalid data provided")
		return
	}

	if len(dropExist.([]bson.M)) > 0 {
		d := dropExist.([]bson.M)[0]
		if d["is_dropped"].(bool) {
			utils.RespondWithError(w, http.StatusBadRequest, "This email has already participated in the airdrop.")
			return
		}
		message := fmt.Sprintf("Use %d as your verification code for the Airdrop", d["code"].(int32))
		nData := notifications.GenericEmailData{
			Formatted: false,
			Content:   message,
		}
		go s.notifiable.SendGeneric(d["email"].(string), "Quicoin Airdrop Verification", nData)
		utils.RespondWithJSON(w, http.StatusCreated, utils.Response{
			Status:  "success",
			Code:    http.StatusOK,
			Message: "Airdrop request created",
		})
		return
	}

	// create drop req
	drop := models.Airdrop{
		ID:        primitive.NewObjectID(),
		Email:     email,
		Code:      utils.GenPasscode(),
		CreatedAt: time.Now().UTC(),
	}

	if err := s.factoryDAO.Insert("airdrop", drop); err != nil {
		log.Printf("err creating airdrop: %v", err)
		utils.RespondWithError(w, http.StatusBadRequest, "An error occurred while processing request")
		return
	}

	// dispatch code to user
	message := fmt.Sprintf("Use %d as your verification code for the Airdrop", drop.Code)
	nData := notifications.GenericEmailData{
		Formatted: false,
		Content:   message,
	}
	go s.notifiable.SendGeneric(drop.Email, "Quicoin Airdrop Verification", nData)

	utils.RespondWithJSON(w, http.StatusCreated, utils.Response{
		Status:  "success",
		Code:    http.StatusCreated,
		Message: "Airdrop request created",
	})
}

// VerifyDropCode verifies an airdrop registration code
func (s *Service) VerifyDropCode(w http.ResponseWriter, r *http.Request) {
	var data map[string]interface{}
	err := utils.DecodeReq(r, &data)
	if err != nil {
		log.Printf("err decoding airdrop_confirm req: %v", err)
		utils.RespondWithError(w, http.StatusBadRequest, "Invalid request data detected")
		return
	}

	email := data["email"].(string)
	code := data["code"].(float64)

	if email == "" || code <= 0 {
		utils.RespondWithError(w, http.StatusBadRequest, "Invalid request data provided")
		return
	}

	drop, err := s.factoryDAO.Query("airdrop", bson.M{
		"email": email,
	})
	if err != nil {
		log.Printf("err retrieving airdrop: %v", err)
		utils.RespondWithError(w, http.StatusBadRequest, "Invalid data provided")
		return
	}

	if len(drop.([]bson.M)) < 1 {
		utils.RespondWithError(w, http.StatusBadRequest, "This email hasn't participated")
		return
	}

	// verifiy code
	d := drop.([]bson.M)[0]
	if d["code"].(int32) != int32(code) {
		utils.RespondWithError(w, http.StatusBadRequest, "Invalid verification code sent")
		return
	}

	d["confirmed"] = true

	if err := s.factoryDAO.Update("airdrop", d["_id"].(primitive.ObjectID), d); err != nil {
		log.Printf("err updating airdrop_confirm: %v", err)
		utils.RespondWithError(w, http.StatusBadRequest, "An error occurred while processing request")
		return
	}

	utils.RespondWithJSON(w, http.StatusCreated, utils.Response{
		Status:  "success",
		Code:    http.StatusOK,
		Message: "Airdrop email confirmed",
	})
}

// ReleaseDrop releases the airdrop to a confirmed user
func (s *Service) ReleaseDrop(w http.ResponseWriter, r *http.Request) {
	var data map[string]interface{}
	err := utils.DecodeReq(r, &data)
	if err != nil {
		log.Printf("err decoding airdrop_release req: %v", err)
		utils.RespondWithError(w, http.StatusBadRequest, "Invalid request data detected")
		return
	}

	email := data["email"].(string)
	walletAddress := data["wallet_address"].(string)

	if email == "" || walletAddress == "" {
		utils.RespondWithError(w, http.StatusBadRequest, "Invalid request data provided")
		return
	}

	drop, err := s.factoryDAO.Query("airdrop", bson.M{
		"email": email,
	})
	if err != nil {
		log.Printf("err retrieving airdrop: %v", err)
		utils.RespondWithError(w, http.StatusBadRequest, "Invalid data provided")
		return
	}

	if len(drop.([]bson.M)) < 1 {
		utils.RespondWithError(w, http.StatusUnauthorized, "Operation not allowed")
		return
	}

	d := drop.([]bson.M)[0]

	if !d["confirmed"].(bool) || d["is_dropped"].(bool) {
		utils.RespondWithError(w, http.StatusUnauthorized, "Operation not allowed")
		return
	}

	// drop is validated and ready for release
	// prepare drop release payload
	icoTradeWallet := os.Getenv("ICO_WALLET")
	icoTradeWalletSecret := os.Getenv("ICO_WALLET_SECRET")
	payload := map[string]string{
		"sender_address":     icoTradeWallet,
		"reciever_address":   walletAddress,
		"amount":             strconv.FormatFloat(airdropReleaseAmount, 'f', -1, 64),
		"sender_private_key": icoTradeWalletSecret,
	}
	err = s.releaseAirdrop(payload)
	if err != nil {
		log.Println(err)
		utils.RespondWithError(w, http.StatusBadRequest, err.Error())
		return
	}

	// mark drop as released
	d["is_dropped"] = true

	if err := s.factoryDAO.Update("airdrop", d["_id"].(primitive.ObjectID), d); err != nil {
		log.Printf("err updating airdrop_confirm: %v", err)
		utils.RespondWithError(w, http.StatusBadRequest, "An error occurred while processing request")
		return
	}

	utils.RespondWithJSON(w, http.StatusCreated, utils.Response{
		Status:  "success",
		Code:    http.StatusOK,
		Message: "Airdrop has been released",
	})
}

func (s *Service) releaseAirdrop(payload map[string]string) error {
	transferEndpoint := os.Getenv("LID_SERVER_ADDR") + "/api/v1/wallet/transfer"
	resp, err := utils.PostToChain(transferEndpoint, payload)
	if err != nil {
		return err
	}
	defer resp.Body.Close()

	if resp.StatusCode >= 300 {
		var d map[string]interface{}
		err = json.NewDecoder(resp.Body).Decode(&d)
		if err != nil {
			log.Printf("failed to post ico_trade_tx to chain: %v", err)
			return errors.New("Failed to read transaction data from the QUI chain")
		}

		return fmt.Errorf("Error posting ico_trade_tx to chain, resp: %v", d)
	}

	return nil
}
