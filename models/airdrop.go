package models

import (
	"time"

	"go.mongodb.org/mongo-driver/bson/primitive"
)

// Airdrop represents an airdrop for to user
type Airdrop struct {
	ID            primitive.ObjectID `json:"id" bson:"_id"`
	Email         string             `json:"email" bson:"email"`
	Code          int                `json:"code" bson:"code"`
	ReferralCode  string             `json:"referral_code" bson:"referral_code"`
	WalletAddress string             `json:"wallet_address" bson:"wallet_address"`
	IsDropped     bool               `json:"is_dropped" bson:"is_dropped"`
	Confirmed     bool               `json:"confirmed" bson:"confirmed"`
	CreatedAt     time.Time          `json:"created_at" bson:"created_at"`
}
